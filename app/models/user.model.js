const mongoose = require('mongoose');
 
const UserSchema = mongoose.Schema({
    firstname: String,
    lastname: String,
    address: String
});
 
module.exports = mongoose.model('Student', UserSchema);