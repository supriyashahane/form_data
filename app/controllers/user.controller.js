const Student = require('../models/user.model.js');
 
// Save FormData - User to MongoDB
exports.save = (req, res) => {
	console.log('Post a User: ' + JSON.stringify(req.body));
	
    // Create a Customer
    const student = new Student({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        address: req.body.address
    });
 
    // Save a Customer in the MongoDB
    student.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
};
 
// Fetch all Users
exports.findAll = (req, res) =>  {
	console.log("Fetch all Users" );
	
    Student.find()
    .then(students => {
        console.log(students);
    }).catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
};